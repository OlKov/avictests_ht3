import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class TradeInPage extends BasePage {

    @FindBy(xpath = "//a[@class='green-color'][contains (text(),'Trade-in оценка')]")
    private WebElement tradeInLink;

    @FindBy(id="nextBtn")
    private WebElement buttonNext;

    @FindBy(xpath = "//div[contains (@class, 'tradein-buttons')]" +
            "/button[contains (@class,'btn-tradein-fullwidth-small')]")
    List<WebElement> buttonList;

    public void tradeInLinkClick(){
        tradeInLink.click();
    }

    public void buttonNextClick(){
        buttonNext.click();
    }


    public int buttonListSize(){
        return buttonList.size();
    }


    public TradeInPage(WebDriver driver) {
        super(driver);
    }
}
