import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class YoutubeIcon extends BasePage{

    @FindBy(xpath="//a[contains(@class, 'footer-soc__item')]//i[contains(@class,'youtube')]")
    private WebElement youtubeIcon;

    public void youtubeIconClick(){
        youtubeIcon.click();
    }

    public YoutubeIcon(WebDriver driver) {
        super(driver);
    }
}
