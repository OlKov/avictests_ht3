import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class BrandBlock extends BasePage{

    @FindBy (xpath="//a[contains(@href,'brand-xiaomi')]")
    WebElement iconXiaomi;

    public void iconXiaomiClick(){
        iconXiaomi.click();
    }

    @FindBy (xpath = "//a[contains(@href, 'https://avic.ua/smartfonyi/proizvoditel--xiaomi')]")
    WebElement linkXiaomi;

    public void linkXiaomiClick(){
        linkXiaomi.click();
    }

    @FindBy (xpath = "//div[contains(@class, 'tags__block')]")
    List<WebElement> tagBlock;

    public BrandBlock(WebDriver driver) {
        super(driver);
    }
}
