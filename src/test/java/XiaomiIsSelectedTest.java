import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import static org.openqa.selenium.By.xpath;

public class XiaomiIsSelectedTest extends BaseTest {

    private final String TAG_ITEM="//a[contains(@class, 'tags__item')]";

    private boolean isTagItemDisplayed(WebElement webElement)
    {
        return webElement.findElement(xpath(TAG_ITEM)).isDisplayed();
    }

    @Test
    public void checkThatOnlyXiaomiIsSelected(){
        getBrandBlock().iconXiaomiClick();
        getBrandBlock().linkXiaomiClick();
        getBrandBlock().implicitWait(30);
        int amount = 0;
        for (WebElement webElement : getBrandBlock().tagBlock) {
            if (isTagItemDisplayed(webElement))
                amount++;
        }
        Assert.assertEquals(amount, 1);
    }
}
