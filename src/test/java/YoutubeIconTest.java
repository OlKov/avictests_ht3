import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.ArrayList;

public class YoutubeIconTest extends BaseTest{

    private static final String YOUTUBE_LINK="www.youtube.com";

    @Test
    public void checkClickOnYoutubeIcon(){
        getYoutubeIcon().youtubeIconClick();
        getYoutubeIcon().implicitWait(30);
        ArrayList<String> tabs = new ArrayList<>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(1));
        Assert.assertTrue(getDriver().getCurrentUrl().contains(YOUTUBE_LINK));
    }
}
