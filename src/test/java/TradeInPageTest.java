import org.testng.Assert;
import org.testng.annotations.Test;

public class TradeInPageTest extends BaseTest {

    private static final int EXPECTED_AMOUNT_OF_BUTTONS = 3;

    @Test
    public void checkThatSecondPageOfTradeInContainsThreeButtons() {
        getTradeInPage().tradeInLinkClick();
        getTradeInPage().buttonNextClick();
        getTradeInPage().implicitWait(30);
        Assert.assertEquals(getTradeInPage().buttonListSize(), EXPECTED_AMOUNT_OF_BUTTONS);
    }
}
